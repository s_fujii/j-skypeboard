package shinsaku.fujii.skypeboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkypeboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkypeboardApplication.class, args);
	}
}
