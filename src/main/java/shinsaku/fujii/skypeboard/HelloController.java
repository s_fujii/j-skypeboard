package shinsaku.fujii.skypeboard;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by LIN on 2017/08/16.
 */
@RestController
@EnableAutoConfiguration
public class HelloController {
    @RequestMapping("/")
    @ResponseBody
    public String index() {
        return "Hello world";
    }
}